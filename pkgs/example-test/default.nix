{ stdenv, pkgs }:

stdenv.mkDerivation {
  name = "example-test";
  installPhase = "";
  version = "1.0";
  src = pkgs.fetchgit {
  "url" = "https://github.com/Arxwel/TP_Shell.git";
  "sha256" = "0m8wn7888grnl9rmvirz57i7ffz7sw9vkgcn2w5qj3f7ak0qnlxv";
  };
} 

