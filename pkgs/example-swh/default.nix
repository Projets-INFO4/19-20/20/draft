{ stdenv, pkgs }:

stdenv.mkDerivation {
  name = "swhtest";
  installPhase = "";
  version = "1.0";
  src = pkgs.fetchswh {
  "swhid" = "977fc4b98c0e85816348cebd3b12026407c368b6";
  };
} 

