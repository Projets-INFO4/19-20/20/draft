#!/bin/bash

retCode=`curl -I -X GET "https://archive.softwareheritage.org/api/1/vault/directory/$1/" | grep "^HTTP\/" | cut -d " " -f 2`

case $retCode in
	200)
		echo "OK"
		curl -X GET "https://archive.softwareheritage.org/api/1/vault/directory/$1/" --output "result-$1.tar.gz"
		exit 0
		;;
	404)
		echo "Not Found"
		curl -X POST "https://archive.softwareheritage.org/api/1/vault/directory/$1/"
		exit 1
		;;
	400)
		echo "Bad Request"
		exit 2
		;;
	*)
		echo "Unknown"
		;;
esac


